import jwt from 'jsonwebtoken'

export const verifyToken = async (req, res, next) => {
  try {
    const secret = "DemoSecret@"
    console.log(req.headers.authorization);
    if (!req.headers.authorization) {
      return res.status(401).send({ "Message": "Token Not Provided!!" })
    }
    console.log(req.headers.authorization, "Authorization");
    const token = req.headers.authorization.split(" ")[1]

    const decoded = jwt.verify(token, secret)
    console.log(decoded);
    req['decoded'] = decoded
    next()
  } catch (error) {
    console.log(error)
    return res.status(500).send(error)
  }

}