import jwt from 'jsonwebtoken'
import axios from 'axios'
export const returnJSON = (req, res) => {
  try {
    return res.status(200).send({
      "task": "Demo",
      "developer": "Yusuf"
    })
  } catch (error) {
    console.log(error);
    return res.status(500).send(error)
  }
}

export const editJSON = async (req, res) => {
  try {
    const json = req.body
    const data = req.decoded
    const response = await axios.get('https://api.publicapis.org/entries')
    const apiData = response.data
    return res.status(201).send({ ...json, "developer": "yusuf", "tokenData": data, "apiData": apiData })
  } catch (error) {
    console.log(error);
    return res.status(500).send(error)
  }
}

export const generateToken = (req, res) => {
  try {
    const secret = "DemoSecret@"
    const payload = {
      "tokenType": "JWT",
      "generatedBy": "Public API"
    }
    const token = jwt.sign(payload, secret)
    console.log(token, "generated token"
    );
    return res.status(200).send({ "token": token })
  } catch (error) {
    console.log(error);
    return res.status(500).send(error)
  }

}