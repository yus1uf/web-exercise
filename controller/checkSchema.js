import bodySchemaValidator from "../helper/schemaValidator.js"
export const checkObjectSchema = (req, res) => {
  const { error, value } = bodySchemaValidator.validate(req.body, {
    abortEarly: false
  })
  if (error) {
    return res.status(200).send(false)
  } else {
    return res.status(200).send(true)
  }
}