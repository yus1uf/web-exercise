import express from 'express'
import { returnJSON, editJSON, generateToken } from '../controller/demoController.js'
import { verifyToken } from '../middleware/verifyJwt.js'
import { checkObjectSchema } from '../controller/checkSchema.js'
const router = express.Router()

router.get('/get-json', verifyToken, returnJSON)
router.post('/post-json', verifyToken, editJSON)
router.get('/generate-token', generateToken)
router.post('/check-schema', checkObjectSchema)

export default router