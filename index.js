import express from 'express'
import router from './routes/demoRoute.js'
const app = express()



app.use(express.json())

app.use('/api', router)


app.listen(4000, () => {
  console.log('App is listening on 4000');
})