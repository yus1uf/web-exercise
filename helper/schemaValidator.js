import Joi from "joi";

const bodySchemaValidator = Joi.object({
  id: Joi.number(),
  name: Joi.string()
})

export default bodySchemaValidator